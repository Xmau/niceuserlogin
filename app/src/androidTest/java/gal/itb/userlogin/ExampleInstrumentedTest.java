package gal.itb.userlogin;

import android.content.Context;
import android.view.View;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void login(){
        onView(withId(R.id.btnLoginWelcome))
                .perform(click());
        onView(withId(R.id.tvUsernameLogin))
                .perform(replaceText("user1"));
        onView(withId(R.id.tvPasswordLogin))
                .perform(replaceText("qwe"));
        onView(withId(R.id.btnLoginLogin))
                .perform(click());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.tvLogged))
                .check(matches(isDisplayed()));
    }
    @Test
    public void loginError(){
        onView(withId(R.id.btnLoginWelcome))
                .perform(click());
        onView(withId(R.id.tvUsernameLogin))
                .perform(replaceText("user2"));
        onView(withId(R.id.tvPasswordLogin))
                .perform(replaceText("qwe"));
        onView(withId(R.id.btnLoginLogin))
                .perform(click());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.tvUsernameLogin))
                .check(matches(isDisplayed()));
    }

    @Test
    public void registerError() {
        onView(withId(R.id.btnRegisterWelcome))
                .perform(click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvUserNameRegister))
                .perform(replaceText("user1"));
        onView(withId(R.id.tvPasswordRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvRepeatPasswordRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvEmailRegister))
                .perform(replaceText("esteban@itb.cat"));
        onView(withId(R.id.btnRegisterRegister))
                .perform(scrollTo(), click());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
    }

    @Test
    public void register(){
        onView(withId(R.id.btnRegisterWelcome))
                .perform(click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvUserNameRegister))
                .perform(replaceText("user2"));
        onView(withId(R.id.tvPasswordRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvRepeatPasswordRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvEmailRegister))
                .perform(replaceText("esteban@itb.cat"));
        onView(withId(R.id.btnRegisterRegister))
                .perform(scrollTo(), click());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.tvLogged))
                .check(matches(isDisplayed()));
    }

    @Test
    public void registerIncorrecto(){
        onView(withId(R.id.btnRegisterWelcome))
                .perform(click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
        onView(withId(R.id.btnRegisterRegister))
                .perform(scrollTo(), click());
        onView(withId(R.id.tvUserNameRegisterLayout))
                .check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.tvPasswordRegisterLayout))
                .check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.tvEmailRegisterLayout))
                .check(matches(hasTextInputLayoutErrorText("Required Field")));
    }
    @Test
    public void registerEmailIncorrecto(){
        onView(withId(R.id.btnRegisterWelcome))
                .perform(click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvUserNameRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvPasswordRegister))
                .perform(replaceText("Esteban"));
        onView(withId(R.id.tvEmailRegister))
                .perform(replaceText("aaaaa"));
        onView(withId(R.id.btnRegisterRegister))
                .perform(scrollTo(), click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvEmailRegisterLayout))
                .check(matches(hasTextInputLayoutErrorText("Email invalid")));
    }

    @Test
    public void navegacionLoginRegisterLogin(){
        onView(withId(R.id.btnLoginWelcome))
                .check(matches(isDisplayed()));
        onView(withId(R.id.btnLoginWelcome))
                .perform(click());

        onView(withId(R.id.btnRegisterLogin))
                .check(matches(isDisplayed()));
        onView(withId(R.id.btnRegisterLogin))
                .perform(click());

        onView(withId(R.id.btnLoginRegister))
                .perform(scrollTo(), click());

        onView(withId(R.id.btnRegisterLogin))
                .perform(click());
        onView(withId(R.id.tvNameRegister))
                .check(matches(isDisplayed()));
    }


    public static Matcher<View> hasTextInputLayoutErrorText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getError();

                if (error == null) {
                    return false;
                }

                String errorString = error.toString();

                return expectedErrorText.equals(errorString);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

}

