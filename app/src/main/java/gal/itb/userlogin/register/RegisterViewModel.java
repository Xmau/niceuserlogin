package gal.itb.userlogin.register;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import gal.itb.userlogin.model.UserForm;
import gal.itb.userlogin.retrofit.repositorio.Repositorio;
import gal.itb.userlogin.retrofit.service.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterViewModel extends AndroidViewModel {
    Service service = Repositorio.getRegister();

    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();

    public RegisterViewModel(@NonNull Application application) {
        super(application);
    }
    public void getCredentials(String user) {
        loading.setValue(true);
        Call<UserForm> call = service.getRegister(user);
        call.enqueue(new Callback<UserForm>() {
            @Override
            public void onResponse(Call<UserForm> call, Response<UserForm> response) {
                if (response.body().isLogged()) {
                    logged.setValue(true);
                } else {
                    error.setValue(response.body().getError());
                }
                loading.setValue(false);
            }

            @Override
            public void onFailure(Call<UserForm> call, Throwable t) {
                throw new RuntimeException(t);
            }
        });
    }
}
