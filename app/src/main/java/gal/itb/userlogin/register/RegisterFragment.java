package gal.itb.userlogin.register;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;


import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.datepicker.MaterialDatePicker;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;


import butterknife.BindView;
import butterknife.ButterKnife;
import gal.itb.userlogin.R;
import gal.itb.userlogin.model.UserForm;


public class RegisterFragment extends Fragment {

    private RegisterViewModel mViewModel;
    @BindView(R.id.btnRegisterRegister)
    Button btnRegisterRegister;

    @BindView(R.id.btnLoginRegister)
    Button btnLoginRegister;

    @BindView(R.id.tvUserNameRegister)
    EditText tvUserNameRegister;

    @BindView(R.id.tvPasswordRegister)
    EditText tvPasswordRegister;

    @BindView(R.id.tvEmailRegister)
    EditText tvEmailRegister;

    @BindView(R.id.tvUserNameRegisterLayout)
    TextInputLayout tvUserNameRegisterLayout;

    @BindView(R.id.tvPasswordRegisterLayout)
    TextInputLayout tvPasswordRegisterLayout;

    @BindView(R.id.tvEmailRegisterLayout)
    TextInputLayout tvEmailRegisterLayout;

    @BindView(R.id.tvBirthRegister)
    TextInputEditText tvBirthRegister;

    @BindView(R.id.tvGenderRegister)
    TextInputEditText tvGenderRegister;

    MutableLiveData<UserForm> userForm;


    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        btnRegisterRegister.setOnClickListener(this::registerRegisterClicked);
        btnLoginRegister.setOnClickListener(this::loginRegisterClicked);
        tvBirthRegister.setOnClickListener(this::onBirthClicked);
        tvGenderRegister.setOnClickListener(this::onGenderClicked);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        // TODO: Use the ViewModel


        mViewModel.loading.observe(this, this::onLoadingResultChanged);
        mViewModel.logged.observe(this,this::onRegisterResultChanged);
        mViewModel.error.observe(this,this::onError);
    }

    private void onError(String s) {
        Snackbar.make(getView(),s,Snackbar.LENGTH_SHORT).show();
    }
    ProgressDialog dialog;
    private void onLoadingResultChanged(Boolean isLoading) {
        if(isLoading){

            dialog = ProgressDialog.show(getContext(), "Titulo", "Texto", isLoading);
            dialog.show();
        } else {
            if(dialog!=null)
                dialog.dismiss();
        }
    }

    private void onRegisterResultChanged(Boolean isLogged) {
        if(isLogged) {
            Navigation.findNavController(getView()).navigate(R.id.action_register_to_logged);
        }
    }

    private void onGenderClicked(View view) {
        String [] optionsArray = {"hombre","mujer"};
        new MaterialAlertDialogBuilder(getContext(), R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered)
                .setTitle("Title")
                .setItems(optionsArray, /* listener */ null)
                .show();

        //Funciona a medias
    }

    private void onBirthClicked(View view) {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Fecha");
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());

    }

    private void doOnDateSelected(Long aLong) {
        Date date =  new Date(aLong);
        tvBirthRegister.setText(date.toString());
    }

    private void loginRegisterClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_register_to_login);
    }

    private void registerRegisterClicked(View view) {
        if(validateIsNotEmpty()){
            mViewModel.getCredentials(tvUserNameRegister.getText().toString());
        }
    }



    private boolean validateIsNotEmpty() {
        boolean valid = true;


        if(tvUserNameRegister.getText().toString().isEmpty()){
            if(valid) {
                scrollTo(tvUserNameRegisterLayout);
            }
            tvUserNameRegisterLayout.setError(getString(R.string.error_isEmpty));
            valid = false;
        }

        if (tvPasswordRegister.getText().toString().isEmpty()){
            if(valid) {
                scrollTo(tvPasswordRegisterLayout);
            }
            valid = false;
            tvPasswordRegisterLayout.setError(getString(R.string.error_isEmpty));
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(tvEmailRegister.getText().toString().trim()).matches()){
            if(valid) {
                scrollTo(tvEmailRegisterLayout);
            }
            valid = false;
            tvEmailRegisterLayout.setError(getString(R.string.error_invalidEmail));

        }

        if (tvEmailRegister.getText().toString().isEmpty()){
            if(valid) {
                scrollTo(tvEmailRegisterLayout);
            }
            valid=false;
            tvEmailRegisterLayout.setError(getString(R.string.error_isEmpty));
        }


        return valid;
    }

    private void scrollTo(TextInputLayout tvUserNameRegisterLayout) {
            tvUserNameRegisterLayout.getParent().requestChildFocus(tvUserNameRegister,tvUserNameRegisterLayout);
    }
}
