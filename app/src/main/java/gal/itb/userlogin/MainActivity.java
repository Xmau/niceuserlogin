package gal.itb.userlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }


        @Override
        public boolean onSupportNavigateUp() {
            // Afegir si es vol mostrar el 'back' a la actionBar
            return findNavController(this, R.id.nav_host_fragment).navigateUp();
        }
}
