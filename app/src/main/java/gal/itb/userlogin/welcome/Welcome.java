package gal.itb.userlogin.welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import gal.itb.userlogin.R;


public class Welcome extends Fragment {

    private WelcomeViewModel mViewModel;

    @BindView(R.id.btnLoginWelcome)
    Button btnLogin;
    @BindView(R.id.btnRegisterWelcome)
    Button btnRegister;

    public static Welcome newInstance() {
        return new Welcome();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WelcomeViewModel.class);
        // TODO: Use the ViewModel
        btnLogin.setOnClickListener(this::loginWelcomeClicked);
        btnRegister.setOnClickListener(this::registerWelcomeClicked);
    }

    private void registerWelcomeClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcome_to_register);
    }

    private void loginWelcomeClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcome_to_login);
    }

}
