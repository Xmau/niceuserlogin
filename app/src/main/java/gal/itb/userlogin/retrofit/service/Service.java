package gal.itb.userlogin.retrofit.service;

import gal.itb.userlogin.model.UserForm;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Service {
    @GET("{user}/login.json")
    Call<UserForm> getLogin(@Path("user") String user);
    @GET("{user}/register.json")
    Call<UserForm> getRegister(@Path("user") String user);
}


