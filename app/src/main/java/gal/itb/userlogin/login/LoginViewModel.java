package gal.itb.userlogin.login;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import gal.itb.userlogin.model.UserForm;
import gal.itb.userlogin.retrofit.repositorio.Repositorio;
import gal.itb.userlogin.retrofit.service.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {
    Service service = Repositorio.getLogin();

    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    // TODO: Implement the ViewModel
    public void getCredentials(String user) {
        loading.setValue(true);
        Call<UserForm> call = service.getLogin(user);
        call.enqueue(new Callback<UserForm>() {
            @Override
            public void onResponse(Call<UserForm> call, Response<UserForm> response) {
                if (response.body().isLogged()) {
                    logged.setValue(true);
                } else {
                    error.setValue(response.body().getError());
                }
                loading.setValue(false);
            }

            @Override
            public void onFailure(Call<UserForm> call, Throwable t) {
                throw new RuntimeException(t);
            }
        });
    }
}
