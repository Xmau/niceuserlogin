package gal.itb.userlogin.login;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import gal.itb.userlogin.R;


public class LoginFragment extends Fragment {

    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @BindView(R.id.btnLoginLogin)
    Button btnLoginLogin;
    @BindView(R.id.btnRegisterLogin)
    Button btnRegisterLogin;
    @BindView(R.id.btnForgotLogin)
    Button btnForgotLogin;

    @BindView(R.id.tvUsernameLogin)
    TextInputEditText tvUserNameLogin;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        btnLoginLogin.setOnClickListener(this::loginLoginClicked);
        btnRegisterLogin.setOnClickListener(this::registerLoginClicked);
        btnForgotLogin.setOnClickListener(this::forgotLoginClicked);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);




        mViewModel.loading.observe(this, this::onLoadingResultChanged);
        mViewModel.logged.observe(this,this::onLoginResultChanged);
        mViewModel.error.observe(this,this::onError);
    }

    private void onError(String s) {
        Snackbar.make(getView(),s,Snackbar.LENGTH_SHORT).show();
    }

    private void forgotLoginClicked(View view) {
    }

    private void registerLoginClicked(View view) {
            Navigation.findNavController(view).navigate(R.id.action_login_to_register);
    }

    private void loginLoginClicked(View view) {
        mViewModel.getCredentials(tvUserNameLogin.getText().toString());
    }

    ProgressDialog  dialog;
    private void onLoadingResultChanged(Boolean isLoading) {
        if(isLoading){

            dialog = ProgressDialog.show(getContext(), "Titulo", "Texto", isLoading);
            dialog.show();
        } else {
            if(dialog!=null)
                dialog.dismiss();
        }
    }

    private void onLoginResultChanged(Boolean isLogged) {
        if(isLogged) {
            Navigation.findNavController(getView()).navigate(R.id.action_login_to_logged);
        }
    }
}
