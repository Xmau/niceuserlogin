package gal.itb.userlogin.model;

public class UserForm {
    private String authToken;
    private String error;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isLogged(){
        return authToken!=null;
    }

}
